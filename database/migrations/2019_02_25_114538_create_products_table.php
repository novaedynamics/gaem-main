<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('sku')->nullable();
            $table->string('currency')->default('PHP ');
            $table->string('long_description')->nullable();
            $table->string('short_description')->nullable();
            $table->decimal('regular_price')->default(0.00);
            $table->decimal('sale_price')->default(0.00);
            $table->decimal('pv')->default(0.00);
            $table->enum('stock_status',['in-stock','out-of-stock','backorder'])->default('in-stock');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
