<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodes', function(Blueprint $table) {
        $table->increments('id');
        $table->decimal('balance')->default(0.00);
        $table->integer('parent_id')->nullable();
        $table->integer('user_id')->unsigned();
        $table->integer('wallet_id')->unsigned()->nullable();
        $table->integer('package_id')->unsigned()->default(1);
        $table->integer('lft')->nullable();
        $table->integer('rgt')->nullable();
        $table->integer('lft_pts')->default(0);
        $table->integer('rgt_pts')->default(0);
        $table->text('data')->nullable();
        $table->integer('depth')->nullable();
        $table->integer('points')->nullable();
        $table->string('name')->unique();
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nodes');
    }
}
