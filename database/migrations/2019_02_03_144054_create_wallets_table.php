<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->increments('id');

            $table->string('source');
            $table->string('description');
            $table->decimal('amount')->default(0.00);
            $table->enum('type',['Dr','Cr']);
            $table->enum('status',['PROCESSING','COMPLETED','DENIED','HOLD']);
            $table->string('currency');
            $table->text('data')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('node_id')->unsigned()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}
