<?php

use Illuminate\Database\Seeder;
use App\Package;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $starter_package = Package::create([
            'name' => 'Starter Package',
            'code' => 'SP',
            'value' => 598.00
        ]);

        $builder_package = Package::create([
            'name' => 'Builder Package',
            'code' => 'BP',
            'value' => 1698.00
        ]);

        $advance_package = Package::create([
            'name' => 'Advance Package',
            'code' => 'AP',
            'value' => 6398.00
        ]);
    }
}
