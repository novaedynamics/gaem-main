<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role_admin = Role::where('name', 'admin')->first();
        $role_manager  = Role::where('name', 'manager')->first();
        $role_regular  = Role::where('name', 'regular')->first();
        $role_inactive  = Role::where('name', 'inactive')->first();

        $super = User::create([
            'firstname' => 'Super',
            'middlename' => 'A',
            'lastname' => 'User',
            'username' => 'superadmin',
            'email' => 'super@d.com',
            'password' => Hash::make('secret')
        ]);

        $super->roles()->attach($role_admin);

        $superA = User::create([
            'firstname' => 'SuperA',
            'middlename' => 'A',
            'lastname' => 'User',
            'username' => 'supera',
            'email' => 'supera@d.com',
            'password' => Hash::make('secret')
        ]);

        $superA->roles()->attach($role_manager);

        $superM = User::create([
            'firstname' => 'SuperM',
            'middlename' => 'A',
            'lastname' => 'User',
            'username' => 'superm',
            'email' => 'superm@d.com',
            'password' => Hash::make('secret')
        ]);

        $superM->roles()->attach($role_manager);

        $corp = User::create([
            'firstname' => 'GAEM',
            'middlename' => 'BP',
            'lastname' => 'CORPORATION',
            'username' => 'gaemcorp',
            'email' => 'gaem1@gaemcorp.com',
            'city' => 'Cebu City',
            'country' => 'Philippines',
            'password' => Hash::make('secret')
        ]);

        $corp->roles()->attach($role_regular);

    }
}
