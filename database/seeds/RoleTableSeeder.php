<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_employee = new Role();
        $role_employee->name = 'admin';
        $role_employee->description = 'A super admin account';
        $role_employee->save();

        $role_manager = new Role();
        $role_manager->name = 'manager';
        $role_manager->description = 'A manager account';
        $role_manager->save();

        $role_regular = new Role();
        $role_regular->name = 'regular';
        $role_regular->description = 'A regular account';
        $role_regular->save();

        $role_inactive = new Role();
        $role_inactive->name = 'inactive';
        $role_inactive->description = 'A inactive account';
        $role_inactive->save();

    }
}
