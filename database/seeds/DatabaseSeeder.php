<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RoleTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(PackagesTableSeeder::class);
         $this->call(NodesTableSeeder::class);
         $this->call(CodesTableSeeder::class);
         $this->call(ProductsTableSeeder::class);
//         $this->call(OrdersTableSeeder::class);
//         $this->call(TransfersTableSeeder::class);
//         $this->call(WalletsTableSeeder::class);
//         $this->call(GiftTableSeeder::class);

    }
}
