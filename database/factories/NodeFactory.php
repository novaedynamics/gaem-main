<?php

use Faker\Generator as Faker;

$factory->define(App\Node::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->userName.time(),
        'user_id' => 1,
        'parent_id' => 0,
        'package_id' => 1,
        'balance' => 0.00,
        'data' => json_encode([
            'match' => ['count' => 0, 'updated_at' => Carbon::now()]
        ])
    ];
});
