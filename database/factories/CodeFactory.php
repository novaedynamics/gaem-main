<?php

use Faker\Generator as Faker;

$factory->define(App\Code::class, function (Faker $faker) {
    return [
        'code' => $faker->unique()->randomNumber(6),
        'status' => 'VALID',
        'package_id' => 1,
        'user_id' => 1,
    ];
});
