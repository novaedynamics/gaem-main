@extends('layouts.backend')

@section('content')
    <header-common page-title="Wallet Referral" main-menu="Wallet" sub-menu="Referral"></header-common>
    {{--<Royalty royalty-data="{{$royalty}}"></Royalty>--}}
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <Block block-title="REFERRAL COMMISSION">
                    <h1>{{config('currency.default')}} {{$referrals}}</h1>
                </Block>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xl-4">
                <a href="/referrals/move" class="btn btn-primary">Move to main wallet</a>
                {{--<a href="/wallet/transactions" class="btn btn-primary">Transactions</a>--}}
            </div>
        </div>
    </div>
@endsection