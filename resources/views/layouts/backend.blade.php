<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>{{config('app.name')}}</title>

        <meta property="og:url"             content="https://dashboard.gaemcorp.net" />
        <meta property="og:image"           content="/preview.jpg" />
        <meta property="og:description"    content="GAEM CORPORATION - Health & Wellness" />
        <meta name="description" content="GAEM CORPORATION - Health & Wellness">
        <meta name="author" content="GAEM CORPORATION">
        <meta name="robots" content="noindex, nofollow">


        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Icons -->
        <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.png') }}">
        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

        <!-- Fonts and Styles -->
        @yield('css_before')
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
        <link rel="stylesheet" id="css-main" href="{{ mix('/css/oneui.css') }}">

        <!-- You can include a specific file from public/css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="{{ mix('/css/themes/amethyst.css') }}"> -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
        @yield('css_after')

        <!-- Scripts -->
        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
    </head>
    <body>

        <div id="page-container" class="sidebar-o enable-page-overlay sidebar-dark side-scroll page-header-fixed">

            <nav id="sidebar" aria-label="Main Navigation">
                <!-- Side Header -->
                <div class="content-header bg-white-5">
                    <!-- Logo -->
                    <div style="margin:auto;">
                    <a class="font-w600 text-dual" href="/">
                        {{--<i class="fa fa-circle-notch text-primary"></i>--}}
                        <img src="/avatar0.png" alt="GAEM CORPORATION" width="32px" height="32px">
                    </a></div>
                    <!-- END Logo -->
                </div>
                <!-- END Side Header -->

                <!-- Side Navigation -->
                <div class="content-side content-side-full">
                    <ul class="nav-main">
                        @if(!in_array(Auth::user()->roles()->first()->name,['inactive']))
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('dashboard') ? ' active' : '' }}" href="/dashboard">
                                <i class="nav-main-link-icon si si-speedometer"></i>
                                <span class="nav-main-link-name">Dashboard</span>
                            </a>
                        </li>

                        <li class="nav-main-item">
{{--                            @if(in_array(Auth::user()->roles()->first()->name,['admin','manager']))--}}
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="nav-main-link-icon si si-wallet"></i>
                                <span class="nav-main-link-name">Genealogy</span>
                            </a>
                            <ul class="nav-main-submenu">
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('genealogy') ? ' active' : '' }}" href="/genealogy">
                                        <span class="nav-main-link-name">View Tree</span>
                                    </a>
                                </li>
{{--                                <li class="nav-main-item">--}}
{{--                                    <a class="nav-main-link{{ request()->is('genealogy/search') ? ' active' : '' }}" href="/genealogy/search">--}}
{{--                                        <span class="nav-main-link-name">Search</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
                            </ul>
{{--                            @else--}}
{{--                            <a class="nav-main-link{{ request()->is('genealogy') ? ' active' : '' }}" href="{{route('genealogy')}}">--}}
{{--                                <i class="nav-main-link-icon si si-users"></i>--}}
{{--                                <span class="nav-main-link-name">Genealogy</span>--}}
{{--                            </a>--}}
{{--                            @endif--}}
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('accounts') ? ' active' : '' }}" href="{{route('accounts')}}">
                                <i class="nav-main-link-icon far fa-user-circle"></i>
                                <span class="nav-main-link-name">Accounts</span>
                            </a>
                        </li>
                        @if(in_array(Auth::user()->roles()->first()->name,['admin']))
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('packages') ? ' active' : '' }}" href="{{route('packages')}}">
                                <i class="nav-main-link-icon fa fa-box-open"></i>
                                <span class="nav-main-link-name">Packages</span>
                            </a>
                        </li>
                        @endif
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('leadership') ? ' active' : '' }}" href="{{route('leadership')}}">
                                <i class="nav-main-link-icon si si-energy"></i>
                                <span class="nav-main-link-name">Leadership</span>
                            </a>
                        </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                                <i class="nav-main-link-icon si si-wallet"></i>
                                <span class="nav-main-link-name">Wallet</span>
                            </a>
                            <ul class="nav-main-submenu">
                                <li class="nav-main-item">
                                    <a class="nav-main-link" href="/wallet">
                                        <span class="nav-main-link-name">Summary</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('wallet/transactions') ? ' active' : '' }}" href="/wallet/transactions">
                                        <span class="nav-main-link-name">Transactions</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('wallet/withdraw') ? ' active' : '' }}" href="/wallet/withdraw">
                                        <span class="nav-main-link-name">Withdraw</span>
                                    </a>
                                </li>
                                @if(in_array(Auth::user()->roles()->first()->name,['admin','manager']))
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('wallet/withdraw/process') ? ' active' : '' }}" href="/wallet/withdraw/process">
                                        <span class="nav-main-link-name">Process Withdrawals</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                                    <i class="nav-main-link-icon fa fa-dollar-sign"></i>
                                    <span class="nav-main-link-name">Commissions</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/accounts">
                                            <span class="nav-main-link-name">Accounts</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/unilevel">
                                            <span class="nav-main-link-name">Unilevel</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/royalty">
                                            <span class="nav-main-link-name">Royalty</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/travel">
                                            <span class="nav-main-link-name">Travel Funds</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link" href="/referrals">
                                            <span class="nav-main-link-name">Referrals</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <li class="nav-main-item">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="false" aria-expanded="true" href="#">
                                <i class="nav-main-link-icon far fa-sticky-note"></i>
                                <span class="nav-main-link-name">Codes</span>
                            </a>
                            <ul class="nav-main-submenu">
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('codes') ? ' active' : '' }}" href="/codes">
                                        <span class="nav-main-link-name">View</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('codes/transfer') ? ' active' : '' }}" href="/codes/transfer">
                                        <span class="nav-main-link-name">Transfer</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('codes/history') ? ' active' : '' }}" href="/codes/history">
                                        <span class="nav-main-link-name">History</span>
                                    </a>
                                </li>
                                @if(in_array(Auth::user()->roles()->first()->name,['admin']))
                                <li class="nav-main-item">
                                    <a class="nav-main-link" href="/codes/generate">
                                        <span class="nav-main-link-name">Generate Code</span>
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </li>
                            <li class="nav-main-item">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="false" aria-expanded="true" href="#">
                                    <i class="nav-main-link-icon fa fa-ticket-alt"></i>
                                    <span class="nav-main-link-name">Discounts</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('gifts') ? ' active' : '' }}" href="/gifts">
                                            <span class="nav-main-link-name">View</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('gifts/transfer') ? ' active' : '' }}" href="/gifts/transfer">
                                            <span class="nav-main-link-name">Transfer</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        <li class="nav-main-item">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="false" aria-expanded="true" href="#">
                                <i class="nav-main-link-icon fab fa-opencart"></i>
                                <span class="nav-main-link-name">Points Value</span>
                            </a>
                            <ul class="nav-main-submenu">
                                @if(in_array(Auth::user()->roles()->first()->name,['admin']))
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('orders') ? ' active' : '' }}" href="/orders">
                                        <span class="nav-main-link-name">View</span>
                                    </a>
                                </li>
                                @endif
                                @if(!in_array(Auth::user()->roles()->first()->name,['regular']))
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('orders/create') ? ' active' : '' }}" href="/orders/new">
                                        <span class="nav-main-link-name">New</span>
                                    </a>
                                </li>
                                @endif
                                @if(!in_array(Auth::user()->roles()->first()->name,['regular']))
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('orders/transfer') ? ' active' : '' }}" href="/orders/transfer">
                                        <span class="nav-main-link-name">Transfer</span>
                                    </a>
                                </li>
                                @endif
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('orders/activate') ? ' active' : '' }}" href="/orders/activate">
                                        <span class="nav-main-link-name">Activate</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @if(in_array(Auth::user()->roles()->first()->name,['admin']))
                        <li class="nav-main-item">
                            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="false" aria-expanded="true" href="#">
                                <i class="nav-main-link-icon fab fa-product-hunt"></i>
                                <span class="nav-main-link-name">Products</span>
                            </a>
                            <ul class="nav-main-submenu">
                                <li class="nav-main-item">
                                    <a class="nav-main-link{{ request()->is('products') ? ' active' : '' }}" href="/products">
                                        <span class="nav-main-link-name">View</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        @endif
                        <li class="nav-main-item open">
                            <a class="nav-main-link" href="/followers">
                                <i class="nav-main-link-icon si si-globe"></i>
                                <span class="nav-main-link-name">Followers</span>
                            </a>
                        </li>
                        @endif
                        @if(Auth::user()->hasAnyRole(['admin','manager']))
                        <li class="nav-main-item open">
                            <a class="nav-main-link" href="/documents">
                                <i class="nav-main-link-icon far fa-address-card"></i>
                                <span class="nav-main-link-name">KYC</span>
                            </a>
                        </li>
                        <li class="nav-main-item open">
                            <a class="nav-main-link" href="/users">
                                <i class="nav-main-link-icon far fa-address-card"></i>
                                <span class="nav-main-link-name">Manage Users</span>
                            </a>
                        </li>
                        @endif
                        <li class="nav-main-heading">Account Type</li>
                        <li class="nav-main-item" style="background-color:rebeccapurple;">
                            <a class="nav-main-link" href="#">
                                <span>{{strtoupper(Auth::user()->roles()->first()->name)}}</span>
                            </a>
                        </li>
                        <li class="nav-main-heading">Server Time: {{ \Carbon\Carbon::now()  }}</li>
                    </ul>
                </div>
                <!-- END Side Navigation -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                <!-- Header Content -->
                <div class="content-header">
                    <!-- Left Section -->
                    <div class="d-flex align-items-center">
                        <!-- Toggle Sidebar -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout()-->
                        <button type="button" class="btn btn-sm btn-dual mr-2 d-lg-none" data-toggle="layout" data-action="sidebar_toggle">
                            <i class="fa fa-fw fa-bars"></i>
                        </button>
                        <!-- END Toggle Sidebar -->

                        <!-- Toggle Mini Sidebar -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout()-->
                        <button type="button" class="btn btn-sm btn-dual mr-2 d-none d-lg-inline-block" data-toggle="layout" data-action="sidebar_mini_toggle">
                            <i class="fa fa-fw fa-ellipsis-v"></i>
                        </button>
                        <!-- END Toggle Mini Sidebar -->

                    </div>
                    <!-- END Left Section -->

                    <!-- Right Section -->
                    <div class="d-flex align-items-center">
                        <!-- User Dropdown -->
                        <div class="dropdown d-inline-block ml-2">
                            <button type="button" class="btn btn-sm btn-dual" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded" src="{{ asset(Auth::user()->avatar) }}" alt="Header Avatar" style="width: 32px;">
                                <span class="d-none d-sm-inline-block ml-1">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</span>
                                <i class="fa fa-fw fa-angle-down d-none d-sm-inline-block"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right p-0 border-0 font-size-sm" aria-labelledby="page-header-user-dropdown">
                                <div class="p-3 text-center bg-primary">
                                    <img class="img-avatar img-avatar48 img-avatar-thumb" src="{{ asset(Auth::user()->avatar) }}" alt="">
                                </div>
                                <div class="p-2">
                                    <h5 class="dropdown-header text-uppercase">User Options</h5>
                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="/profile">
                                        <span>Profile</span>
                                        <span>
                                            {{--<span class="badge badge-pill badge-success">1</span>--}}
                                            <i class="si si-user ml-1"></i>
                                        </span>
                                    </a>
                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="/profile/settings">
                                        <span>Settings</span>
                                        <i class="si si-settings"></i>
                                    </a>
                                    <div role="separator" class="dropdown-divider"></div>
                                    @if(Auth::user()->isVerified == "Unverified")
                                    <a href="/accounts/verification">
                                        <h5 class="dropdown-header text-uppercase" style="color:red;">Account {{Auth::user()->isVerified}}</h5>
                                    </a>
                                    @endif
                                    @if(Auth::user()->isVerified == "Verified")
                                        <h5 class="dropdown-header text-uppercase" style="color:green;">Account {{Auth::user()->isVerified}}</h5>
                                    @endif
                                    <h5 class="dropdown-header text-uppercase">Actions</h5>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END User Dropdown -->



                        <!-- Toggle Side Overlay -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        {{--<button type="button" class="btn btn-sm btn-dual ml-2" data-toggle="layout" data-action="side_overlay_toggle">--}}
                            {{--<i class="fa fa-fw fa-list-ul fa-flip-horizontal"></i>--}}
                        {{--</button>--}}
                        <!-- END Toggle Side Overlay -->
                    </div>
                    <!-- END Right Section -->
                </div>
                <!-- END Header Content -->

                <!-- Header Search -->
                <div id="page-header-search" class="overlay-header bg-white">
                    <div class="content-header">
                        <form class="w-100" action="/dashboard" method="POST">
                            @csrf
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                    <button type="button" class="btn btn-danger" data-toggle="layout" data-action="header_search_off">
                                        <i class="fa fa-fw fa-times-circle"></i>
                                    </button>
                                </div>
                                <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                            </div>
                        </form>
                   </div>
                </div>
                <!-- END Header Search -->

                <!-- Header Loader -->
                <!-- Please check out the Loaders page under Components category to see examples of showing/hiding it -->
                <div id="page-header-loader" class="overlay-header bg-white">
                    <div class="content-header">
                        <div class="w-100 text-center">
                            <i class="fa fa-fw fa-circle-notch fa-spin"></i>
                        </div>
                    </div>
                </div>
                <!-- END Header Loader -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                @yield('content')
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="bg-body-light">
                <div class="content py-3">
                    <div class="row font-size-sm">
                        {{--<div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">--}}
                            {{--Crafted with <i class="fa fa-heart text-danger"></i> by <a class="font-w600" href="https://1.envato.market/ydb" target="_blank">pixelcave</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-6 order-sm-1 py-1 text-center text-sm-left">--}}
                            {{--<a class="font-w600" href="https://1.envato.market/xWy" target="_blank">OneUI</a> &copy; <span data-toggle="year-copy">2018</span>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </footer>
            <!-- END Footer -->

        </div>
        <!-- END Page Container -->

        <!-- OneUI Core JS -->
        <script src="{{ mix('js/oneui.app.js') }}"></script>

        <!-- Laravel Scaffolding JS -->
        <script src="{{ mix('js/laravel.app.js') }}"></script>
        
        @yield('js_after')

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136591901-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-136591901-1');
        </script>

    </body>
</html>
