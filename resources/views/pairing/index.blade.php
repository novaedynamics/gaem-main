@extends('layouts.backend')

@section('content')
    <header-common page-title="Wallet Pairing" main-menu="Wallet" sub-menu="Pairing"></header-common>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <Block block-title="ROYALTY COMMISSION">
                    <h1>{{config('currency.default')}}{{$pairing}}</h1>
                </Block>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xl-4">
                @if($pairing > 1000)
                    <a href="/royalty/move" class="btn btn-primary">Move to main wallet</a>
                @else
                    <p><i>minimum move out wallet is {{config('currency.default')}}{{config('commissions.match.minimum.royalty')}}</i></p>
                @endif
                {{--<a href="/wallet/transactions" class="btn btn-primary">Transactions</a>--}}
            </div>
        </div>
    </div>
@endsection