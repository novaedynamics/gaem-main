@extends('layouts.backend')

@section('content')
    <header-common page-title="Wallet Royalty" main-menu="Wallet" sub-menu="Royalty"></header-common>
    {{--<Royalty royalty-data="{{$royalty}}"></Royalty>--}}
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <Block block-title="ROYALTY COMMISSION">
                    <h1>{{config('currency.default')}}{{$royalty}}</h1>
                </Block>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xl-12">
                @if($royalty >= (double) config('withdraw.minimum.royalty') && $button_open == true)
                    <a href="/royalty/move" class="btn btn-primary">Move to main wallet</a>
                @endif
                    <p><strong>Note: </strong><i>minimum move out to main wallet is {{config('currency.default')}}{{config('withdraw.minimum.royalty')}}</i>. Next schedule of move to main wallet is </p>
            </div>
        </div>
    </div>
@endsection