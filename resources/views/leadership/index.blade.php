@extends('layouts.backend')

@section('content')

    <header-common page-title="Leadership Qualifiers" main-menu="Leadership" sub-menu="Qualifiers"></header-common>

            <div class="col-xl-12">
                <div class="block">
                    <div class="block-content">
                        <table class="table table-vcenter">
                            <thead class="thead-light">
                            <tr>
                                <th class="d-none d-sm-table-cell">User</th>
                                <th class="d-none d-sm-table-cell text-center">Direct Invite</th>
                                <th class="d-none d-sm-table-cell text-center">Matched Points</th>
                                {{--<th class="text-center">Activated At</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($leadership) < 1)
                            <tr>
                                <td colspan="4" class="text-center">No records found</td>
                            </tr>
                            @endif
                            @foreach($leadership as $member)
                            <tr><td>
                                    <a class="media py-3" href="javascript:void(0)">
                                        <div class="mr-3 ml-2 overlay-container overlay-bottom">
                                            <img class="img-avatar img-avatar48" src="{{ $member->avatar }}" alt="">
                                            {{--<span class="overlay-item item item-tiny item-circle border border-2x border-white bg-muted"></span>--}}
                                        </div>
                                        <div class="media-body">
                                            <div class="font-w600">{{$member->username}}</div>
                                            <div class="font-w400 font-size-sm text-muted">{{$member->city}}, {{$member->country}}</div>
                                        </div>
                                    </a>
                                </td>
                                <td class="text-center" style="width:25%;">
                                    {{ $member->childrenWeek->count() }}
                                </td>
                                <td class="text-center" style="width:25%;">
                                    {{ $member->walletPairing->count() }}
                                </td>
                            </tr>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

@endsection