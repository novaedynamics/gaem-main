@extends('layouts.backend')

@section('content')

    <header-common page-title="Products" main-menu="Products"></header-common>
    <Products products-data="{{$products}}"></Products>
@endsection