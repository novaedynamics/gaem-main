@extends('layouts.backend')

@section('content')
    <header-common page-title="Genealogy" main-menu="Genealogy"></header-common>

    <div class="content">
        <div class="block">
            <div class="block-content">

                    <table class="table table-bordered" id="genealogy">
                        <thead>
                        <tr>
                            <th scope="col">Account Name</th>
                            <th scope="col">Owner</th>
                            <th scope="col">Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($genealogies as $genealogy)
                        <tr>
                            <td><a href="/genealogy/{{$genealogy->name}}">{{$genealogy->name}}</a></td>
                            <td><a href="/users/{{$genealogy->user->id}}">{{$genealogy->user->firstname}} {{$genealogy->user->lastname}}</a></td>
                            <td>{{$genealogy->created_at}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

            </div>
        </div>
    </div>

@endsection

@section('js_after')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#genealogy').DataTable();
        } );
    </script>
@endsection