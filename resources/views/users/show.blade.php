@extends('layouts.backend')

@section('content')
    <header-common page-title="User Detail" main-menu="User" sub-menu="Detail"></header-common>

    <div class="container">
        <div class="mt-7 mb-5">
            <form>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" readonly class="form-control-plaintext" id="first_name" value="{{$user->firstname}}" />
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" readonly class="form-control-plaintext" id="last_name" value="{{$user->lastname}}" />
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control-plaintext" id="email" value="{{$user->email}}" aria-describedby="emailHelp" placeholder="Enter email">
                        <small id="emailHelp" readonly class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control-plaintext" id="phone" value="{{$user->phone}}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" readonly class="form-control-plaintext" id="address" value="{{$user->address}}" />
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" readonly class="form-control-plaintext" id="city" value="{{$user->city}}" />
                    </div>
                    <div class="form-group">
                        <label for="state">State / Province</label>
                        <input type="text" readonly class="form-control-plaintext" id="state" value="{{$user->state}}" />
                    </div>
                    <div class="form-group">
                        <label for="postcode">Postal Code</label>
                        <input type="text" readonly class="form-control-plaintext" id="postcode" value="{{$user->postal}}" />
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <input type="text" readonly class="form-control-plaintext" id="country" value="{{$user->country}}" />
                    </div>
                </div>
                </div>
                <a href="/users/{{$user->id}}/edit" class="btn btn-primary">Edit</a>
                <a href="/users" class="btn btn-primary">View All Users</a>

            </form>
        </div>
    </div>



@endsection
