@extends('layouts.backend')

@section('content')
    <header-common page-title="Users" main-menu="Users" sub-menu="View"></header-common>

    <div class="content">
        <div class="block">
        <div class="block-content">
        <div class="table-responsive">
            <table id="users_table" class="table table-vcenter">
                <thead class="thead-light">
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td><a href="/users/{{$user->id}}">{{$user->firstname}} {{$user->last_ame}}</a></td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone}}</td>
                        <td>{{$user->city}}, {{$user->province}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>
        </div>

    </div>

@endsection

@section('js_after')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function () {
    $('#users_table').DataTable();
} );
</script>
@endsection