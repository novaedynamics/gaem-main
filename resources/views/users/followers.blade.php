@extends('layouts.backend')

@section('content')
    <header-common page-title="Followers" main-menu="Followers"></header-common>
    <Followers followers="{{ $followers }}"></Followers>
@endsection