@extends('layouts.backend')

@section('content')
    <header-common page-title="Accounts" main-menu="Accounts" sub-menu="Edit"></header-common>
    <div class="content"><account-edit account-name="{{$accountName}}" account-id="{{ $account->id }}" ></account-edit></div>
@endsection