@extends('layouts.backend')

@section('content')
    <header-common page-title="Accounts" main-menu="Accounts"></header-common>
        <div class="col-xl-12 ">
            <div class="block">
                <div class="block-content">
                    <div class="table-responsive">
                    <table class="table table-vcenter">
                        <thead class="thead-light">
                        <tr>
                            <th>Account Name</th>
                            <th>Package</th>
                            <th>Balance</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(count($accounts) < 1)
                        <tr>
                            <td colspan="4" class="text-center">No records found</td>
                        </tr>
                        @endif

                        @if(count($accounts) > 0)
                        @foreach($accounts as $acct)
                        <tr>
                            <td class="font-w600 font-size-sm">
                                {{$acct->name}}
                            </td>
                            <td class="font-w600 font-size-sm">
                                <span class="badge badge-warning">{{$acct->package->name}}</span>
                            </td>
                            <td class="font-w600 font-size-sm">
                                <span class="badge badge-warning">{{config('currency.default')}}{{number_format($acct->wallet->sum('amount'), 2, '.', ',')}}</span>
                            </td>
                            <td class="font-w600 font-size-sm text-center">
                                <div class="btn-group">
                                    @if($acct->wallet->sum('amount') >= config('accounts.balance.cashout.minimum'))
                                    <a href="/accounts/transfer/{{$acct->name}}" class="btn btn-sm btn-light js-tooltip-enabled" data-toggle="tooltip" title="Transfer To Wallet" data-original-title="Edit Client">
                                        <i class="fa fa-fw fa-dollar-sign"></i>
                                    </a>
                                    @endif
                                    <a href="/accounts/edit/{{$acct->id}}" class="btn btn-sm btn-light js-tooltip-enabled" data-toggle="tooltip" title="Edit Account Name" data-original-title="Edit Client">
                                        <i class="fa fa-fw fa-pencil-alt"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <p><strong>Node:</strong> minimum move out to main wallet is {{config('currency.default')}}{{config('withdraw.minimum.pairing')}}</p>
        </div>

@endsection
