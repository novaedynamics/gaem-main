@extends('layouts.backend')

@section('content')
    <header-common page-title="Accounts Balance Transfer" main-menu="Accounts" sub-menu="Balance Transfer"></header-common>

    <div class="col-md-12">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Transferable Amount</h3>
            </div>
            <div class="block-content block-content-full">
                <div class="row">
                    <div class="col-lg-4">
                        <h1 style="">{{config('currency.default')}} {{$amount}}</h1>
                    </div>
                </div>
            </div>
        </div>
        @if($amount >= config('accounts.balance.cashout.minimum'))
        <div class="col-sm-6 col-xl-4" style="padding-left:0;">
            <a href="/accounts/move/{{$node->name}}" class="btn btn-primary">Move To Main Wallet</a>
        </div>
        @endif
    </div>


@endsection