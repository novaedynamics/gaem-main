@extends('layouts.backend')

@section('content')
    <head-avatar avatar="{{asset(Auth::user()->avatar)}}"></head-avatar>

    <div class="content">
        <div class="row">
            {{--start--}}
            <div class="col-md-12">

                <div class="block">
                    <ul class="nav nav-tabs nav-tabs-block" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="account" aria-selected="true">Account</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Avatar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Security</a>
                        </li>
                    </ul>
                    <div class="block-content tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="account" role="tabpanel" aria-labelledby="account-tab">

                            <div class="col-md-12">
                                <account-form user-data="{{ json_encode(auth()->user()) }}"></account-form>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <account-avatar></account-avatar>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="col-md-12">
                                <password-form></password-form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--end--}}
        </div>
    </div>

@endsection