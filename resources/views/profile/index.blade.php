@extends('layouts.backend')

@section('content')
    <head-avatar avatar="{{Auth::user()->avatar}}" fullname="{{Auth::user()->firstname}} {{Auth::user()->lastname}}" account-type="{{Auth::user()->roles()->first()->name}}"></head-avatar>
@endsection
