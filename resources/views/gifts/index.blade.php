@extends('layouts.backend')

@section('content')
    <header-common page-title="Discount Codes" main-menu="Discount" sub-menu="Codes"></header-common>
    {{--<gifts gifts-data="{{ $gifts }}"></gifts>--}}
    <div class="container">
        <div class="row mt-4">
            <div class="col-md-6">
                <Block block-title="AVAILABLE CODES">
                    <h1>{{$gifts}} <span class="font-size-sm">@ {{config('currency.default')}}100 ea</span></h1>
                </Block>
            </div>
        </div>
    </div>
@endsection
