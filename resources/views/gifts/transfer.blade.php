@extends('layouts.backend')

@section('content')
    <header-common page-title="Discount Transfer" main-menu="Discount" sub-menu="Transfer"></header-common>
    <gifts-transfer></gifts-transfer>
@endsection