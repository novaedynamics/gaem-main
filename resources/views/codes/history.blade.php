@extends('layouts.backend')

@section('content')
    <header-common page-title="Code History" main-menu="Code" sub-menu="History"></header-common>
    <code-history transfers="{{$transfers}}" user-id="{{Auth::user()->id}}"></code-history>
@endsection