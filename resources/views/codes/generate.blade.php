@extends('layouts.backend')

@section('content')
    <header-common page-title="Code Generate" main-menu="Code" sub-menu="Generate"></header-common>
    <div class="content"><code-generate></code-generate></div>
@endsection