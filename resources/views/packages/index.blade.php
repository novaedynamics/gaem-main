@extends('layouts.backend')

@section('content')

    <header-common page-title="Packages" main-menu="Packages"></header-common>
    <Packages packages="{{  $packages  }}" />

@endsection