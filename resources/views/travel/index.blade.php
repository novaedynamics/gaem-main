@extends('layouts.backend')

@section('content')

    <header-common page-title="Wallet Travel Funds" main-menu="Wallet" sub-menu="Travel Funds"></header-common>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <Block block-title="TRAVEL FUNDS">
                    <h1>{{config('currency.default')}}{{$travel}}</h1>
                </Block>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xl-12">
                <p><i>Reserve for your future Cruise Travel Experience</i></p>
            </div>
        </div>
    </div>

@endsection