@extends('layouts.backend')

@section('content')
    <header-common page-title="New Order" main-menu="Orders" sub-menu="New"></header-common>
    <new-order products-data="{{ $products }}"></new-order>

@endsection