@extends('layouts.backend')

@section('content')
    <header-common page-title="Activate Order" main-menu="Activate" sub-menu="Order"></header-common>
    <activate-order orders-data="{{ $orders  }}"></activate-order>
@endsection