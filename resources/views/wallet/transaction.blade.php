@extends('layouts.backend')

@section('content')
    <header-common page-title="Wallet Transactions" main-menu="Wallet" sub-menu="Transactions"></header-common>
    <wallet-transaction transactions="{{ $transactions }}"></wallet-transaction>
@endsection