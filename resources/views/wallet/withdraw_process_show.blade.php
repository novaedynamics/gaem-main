@extends('layouts.backend')

@section('content')
    <header-common page-title="Process Withdrawal" main-menu="Wallet" sub-menu="Process Withdrawal"></header-common>

    <div class="content">
        <div class="row">
            <withdraw-complete payout-data="{{ json_encode($details) }}" more-data="{{ json_encode($additional_data) }}"></withdraw-complete>
        </div>
    </div>

@endsection