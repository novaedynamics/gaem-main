@extends('layouts.backend')

@section('content')
    <header-common page-title="Process Withdrawals" main-menu="Wallet" sub-menu="Process Withdrawals"></header-common>

    <div class="col-xl-12">
        <div class="block">
            <div class="block-content">
                <div class="table-responsive">
                <table class="table table-vcenter">
                    <thead class="thead-light">
                    <tr>
                        <th class="d-none d-sm-table-cell">Description</th>
                        <th class="text-center">Source</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Amount</th>
                        <th class="text-center">Transaction Date</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($withdrawals) < 1)
                    <tr>
                        <td colspan="5" class="text-center">No records found</td>
                    </tr>
                    @else
                    @foreach($withdrawals as $w)
                    <tr >
                        <td class="font-w600 font-size-sm">
                            {{--{{ withdrawal.description }}--}}
                            {{ $w->description }}
                        </td>
                        <td class="font-w600 font-size-sm text-center">
                            {{--{{ withdrawal.source }}--}}
                            {{ $w->source }}
                        </td>
                        <td class="font-w600 font-size-sm text-center">
                            {{--{{ withdrawal.status }}--}}
                            {{ $w->status }}
                        </td>
                        <td class="font-w600 font-size-sm text-center">
                            {{--{{ withdrawal.amount }}--}}
                            {{ $w->amount }}
                        </td>
                        <td class="font-w600 font-size-sm text-center">
                            {{--{{ withdrawal.created_at | datePretty }}--}}
                            {{ $w->created_at }}
                        </td>
                        <td class="font-w600 font-size-sm text-center">
                            <div class="btn-group btn-group-sm" role="group" aria-label="Horizontal Primary">
                                <a href="/wallet/withdraw/process/{{$w->id}}" class="btn btn-sm btn-light"><i class="fa fa-ellipsis-h"></i></a>
                                <!--<button type="button" class="btn btn-sm btn-light"><i class="fa fa-check"></i></button>-->
                                <!--<button type="button" class="btn btn-sm btn-light"><i class="fa fa-minus"></i></button>-->
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                </table>
                </div>

                {{ $withdrawals->links() }}
            </div>
        </div>
    </div>

@endsection