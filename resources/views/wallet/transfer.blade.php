@extends('layouts.backend')

@section('content')
    <header-common page-title="Account Balance Transfer" main-menu="Accounts" sub-menu="Balance Transfer"></header-common>
    <div class="col-md-12">
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Transferable Amount</h3>
            </div>
            <div class="block-content block-content-full">
                <div class="row">
                    <div class="col-lg-4">
                        <h1 style="">{{config('currency.default')}} {{$amount}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection