@extends('layouts.backend')

@section('content')
    <header-common page-title="Identification" main-menu="Identification"></header-common>

    <div class="col-md-12">
        <div class="block">
            <ul class="nav nav-tabs nav-tabs-block js-tabs-enabled" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active show" href="#btabs-static-home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link show" href="#btabs-static-profile">Profile</a>
                </li>
                <li class="nav-item ml-auto">
                    <a class="nav-link" href="#btabs-static-settings">
                        <i class="si si-settings"></i>
                    </a>
                </li>
            </ul>
            <div class="block-content tab-content">
                <div class="tab-pane active show" id="btabs-static-home" role="tabpanel">
                    <h4 class="font-w400">Home Content</h4>
                    <p>...</p>
                </div>
                <div class="tab-pane show" id="btabs-static-profile" role="tabpanel">
                    <h4 class="font-w400">Profile Content</h4>
                    <p>...</p>
                </div>
                <div class="tab-pane" id="btabs-static-settings" role="tabpanel">
                    <h4 class="font-w400">Settings Content</h4>
                    <p>...</p>
                </div>
            </div>
        </div>
    </div>

@endsection
