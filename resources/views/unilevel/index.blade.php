@extends('layouts.backend')

@section('content')
    <header-common page-title="Wallet Unilevel" main-menu="Wallet" sub-menu="Unilevel"></header-common>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <Block block-title="UNILEVEL COMMISSION">
                    <h1>{{config('currency.default')}}{{$unilevel}}</h1>
                </Block>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-xl-12">
                @if($unilevel > config('withdraw.minimum.unilevel') && $date_today == $month_end)
                    <a href="/unilevel/move" class="btn btn-primary">Move to main wallet</a>
                @endif
                    <p class="mt-2"><strong>Note: </strong><i>minimum move out to main wallet is {{config('currency.default')}}{{config('withdraw.minimum.unilevel')}}</i></p>
            </div>
        </div>
    </div>
@endsection