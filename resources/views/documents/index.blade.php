@extends('layouts.backend')

@section('content')
    <header-common page-title="Documents View" main-menu="Documents" sub-menu="View"></header-common>

    <div class="content">
        <form action="/documents/search" method="GET">
            <div class="row">
                <div class="col-lg-4">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-search mr-1"></i> Search
                                </button>
                            </div>
                            <input autocomplete="off" value="{{$search}}" name="search" style="background-color: white;" type="text" class="form-control form-control-alt font-size-sm">
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <documents-view documents-data="{{ json_encode($documents) }}" ></documents-view>

        {{ $documents->links() }}
    </div>



@endsection