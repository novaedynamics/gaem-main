
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import { Form, HasError, AlertError } from 'vform';
import VueSweetalert2 from 'vue-sweetalert2';
import moment from 'moment';
import Clipboard from 'v-clipboard';

window.Vue = require('vue');
window.Form = Form;

Vue.filter('datePretty', function (value) {
    return moment(value).format("MMM Do YY");
});

Vue.filter('currencyPHP', function (value) {
    return '₱' + value;
});

// Code status

Vue.use(VueSweetalert2);
Vue.use(Clipboard);
// Vue.use(BootstrapVue);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

Vue.component('Royalty', require('./components/Royalty/Royalty.vue'));
Vue.component('Wallet', require('./components/Wallet/Wallet.vue'));
Vue.component('WalletWithdraw', require('./components/Wallet/Withdraw/Withdraw.vue'));
Vue.component('WalletTransaction', require('./components/Wallet/Transactions/Transactions.vue'));
Vue.component('WithdrawView', require('./components/Wallet/Withdraw/WithdrawView.vue'));
Vue.component('WithdrawComplete', require('./components/Wallet/Withdraw/WithdrawComplete.vue'));
Vue.component('Gifts', require('./components/Gifts/Gifts.vue'));
Vue.component('GiftsTransfer', require('./components/Gifts/Transfer/Transfer.vue'));
Vue.component('Codes', require('./components/Codes/Codes.vue'));
Vue.component('CodeGenerate', require('./components/Codes/CodeGenerate.vue'));
Vue.component('CodeTransfer', require('./components/Codes/CodeTransfer.vue'));
Vue.component('CodeHistory', require('./components/Codes/CodeHistory.vue'));
Vue.component('Followers', require('./components/Users/Followers/Followers.vue'));
Vue.component('Packages', require('./components/Packages/Packages.vue'));
Vue.component('Accounts', require('./components/Accounts/Accounts.vue'));
Vue.component('AccountEdit', require('./components/Accounts/AccountEdit.vue'));
Vue.component('AccountActivate', require('./components/Accounts/Activate/Activate.vue'));
Vue.component('HeaderCommon', require('./components/Common/Header.vue'));
Vue.component('HeadBg', require('./components/Common/HeadBg/HeadBg.vue'));
Vue.component('HeadAvatar', require('./components/Common/HeadAvatar/HeadAvatar.vue'));
Vue.component('Clipboard', require('./components/Common/Clipboard/Clipboard.vue'));
Vue.component('AccountForm', require('./components/Accounts/AccountForm.vue'));
Vue.component('PasswordForm', require('./components/Accounts/PasswordForm.vue'));
Vue.component('AccountAvatar', require('./components/Accounts/Avatar/Avatar.vue'));
Vue.component('Genealogy', require('./components/Genealogy/Genealogy.vue'));
Vue.component('NodeUser', require('./components/Genealogy/Node/Node.vue'));
Vue.component('Products', require('./components/Products/Products.vue'));
Vue.component('Orders', require('./components/Orders/Orders.vue'));
Vue.component('NewOrder', require('./components/Orders/NewOrder/NewOrder.vue'));
Vue.component('TransferOrder', require('./components/Orders/TransferOrder/TransferOrder.vue'));
Vue.component('ActivateOrder', require('./components/Orders/ActivateOrder/ActivateOrder.vue'));

//Users
Vue.component('UserEdit', require('./components/Users/UserEdit.vue'));

Vue.component('DocumentsView', require('./components/Documents/Documents.vue'));

// Common Block
Vue.component('Block', require('./components/Common/Block/Block.vue'));
Vue.component('DocumentUpload', require('./components/Accounts/Verification/Document/DocumentUpload.vue'));


const app = new Vue({
    el: '#page-container'
});

