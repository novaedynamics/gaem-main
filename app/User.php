<?php

namespace App;

use Carbon\Carbon;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Auth;
use App\Document;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'middlename','lastname', 'email',
        'password', 'parent_id','username','avatar',
        'phone','address','city','state','postal','country'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot()
    {

        parent::boot();

        self::creating(function($model){
            $model->avatar = '/media/avatars/avatar0.png';
            $model->pv = 160;
            $model->data = json_encode([
                'pv' => 160,
                'activated' => true,
                'pool' => false,
                'updated_at' => Carbon::today()->format('Y-m-d H:i:s')
            ]);
        });

    }

    public function nodes() {
        return $this->hasMany(\App\Node::class);
    }

    public function codes() {
        return $this->hasMany(\App\Code::class);
    }

    public function gifts() {
        return $this->hasMany(\App\Gift::class);
    }

    public function transfers() {
        return $this->hasMany(\App\Transfer::class);
    }

    public function wallet() {
        return $this->hasMany(\App\Wallet::class);
    }

    public function roles()
    {
        return $this->belongsToMany(\App\Role::class);
    }

    public function parent()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function children()
    {
        return $this->hasMany(\App\User::class, 'parent_id','id');
    }

    public function childrenWeek()
    {
        $today = Carbon::today()->subDay(1);
        return $this->hasMany(\App\User::class, 'parent_id','id')
            ->whereBetween('created_at',[
                $today->startOfWeek()->format('Y-m-d H:i:s'),
                $today->endOfWeek()->format('Y-m-d H:i:s')]);
    }

    public function nodeBuilder() {
        return $this->nodes()->where('package_id',2);
    }

    public function walletPairing() {

        $today = Carbon::today()->subDay(1);
        $start = $today->startOfWeek()->format('Y-m-d H:i:s');
        $end = $today->endOfWeek()->format('Y-m-d H:i:s');

        return $this->wallet()
                    ->where([
            'type' => 'Cr',
            'description' => 'MATCH COMMISSION',
            'source' =>'pairing'])
            ->whereBetween('created_at',[$start,$end]);
    }

    /**
     * @param string|array $roles
     */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) ||
                abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) ||
            abort(401, 'This action is unauthorized.');
    }
    /**
     * Check multiple roles
     * @param array $roles
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }
    /**
     * Check one role
     * @param string $role
     */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    public function documents() {
        return $this->hasMany(Document::class);
    }

    public function getIsVerifiedAttribute() {

        $front  = Document::where(['status' => 'APPROVED','type' => 'FID','user_id' => $this->id])->count();
        $back   = Document::where(['status' => 'APPROVED','type' => 'BID','user_id' => $this->id])->count();
        $selfie = Document::where(['status' => 'APPROVED','type' => 'SI','user_id' => $this->id])->count();

        if($front > 0 && $back > 0 && $selfie > 0) {
            return 'Verified';
        }

        return 'Unverified';
    }

    public function genealogies() {
        return $this->hasMany(Genealogy::class);
    }

}
