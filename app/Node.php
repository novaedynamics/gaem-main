<?php

namespace App;
//use Baum\Node as Baum;
use App\Package;
use App\User;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{

    public $fillable = [
        'balance',
        'parent_id',
        'user_id',
        'package_id',
        'lft',
        'rgt',
        'lft_pts',
        'rgt_pts',
        'lcount',
        'rcount',
        'depth',
        'data',
        'name'
    ];

    public static function boot()
    {

        parent::boot();

        self::creating(function($model){
            $model->data =  json_encode([
                'match' => ['count' => 0, 'updated_at' => Carbon::today()]
            ]);
        });

    }

    public function wallet() {
        return $this->hasMany(\App\Wallet::class);
    }

    public function user() {
        return $this->belongsTo(\App\User::class);
    }

    public function package() {
        return $this->belongsTo(\App\Package::class);
    }

    public function child_lft() {
        return $this->hasMany('App\Genealogy','lft','id')->with(['child_lft']);
    }

    public function child_rgt() {
        return $this->hasMany('App\Genealogy','rgt','id')->with(['child_lft','child_rgt']);
    }

    public function setLft($id = null) {
        return $this->update(['lft' => $id]);
    }

    public function setRgt($id = null) {
        return $this->update(['rgt' => $id]);
    }


}
