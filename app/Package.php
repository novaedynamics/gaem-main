<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function nodes() {
        return $this->hasMany(App\Node::class);
    }

    public function codes() {
        return $this->hasMany(App\Codes::class);
    }

    public function transfers() {
        return $this->hasMany(App\Transfer::class);
    }

}
