<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Gift extends Model
{
    public $fillable = ['code','amount','status','user_id'];

    public function user() {
        return $this->belongsTo(App\User::class);
    }
}
