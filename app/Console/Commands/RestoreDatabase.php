<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class RestoreDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:restore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restore database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            \Log::info("Restoring database");
            $data = storage_path('backups/latest/database.sql');
            DB::unprepared($data);
            $this->info('The restore backup has been proceed successfully.');
        } catch (ProcessFailedException $exception) {
            $this->error('The restore backup process has been failed.');
        }

    }
}
