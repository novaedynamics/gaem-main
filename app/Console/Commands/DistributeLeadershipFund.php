<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\Wallet;
use Carbon\Carbon;
use Mockery\Exception;

class DistributeLeadershipFund extends Command
{

    public $amount;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leadership:dispatch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute and distribute the funds for leadership.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->amount = $this->get_amount_to_distribute();
//        $this->amount = 0;

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->isTodayStartOfWeek()) {
            if($this->users()->count() < 1) return;
            if($this->amount < 1) return;
            if($this->isDispatched()) return;
            $this->distribute_funds();
        }
    }


    // All qualifiers
    public function users() {
        $users = User::with(['childrenWeek','nodeBuilder','walletPairing'])
            ->has('childrenWeek','>', (int) config('commissions.leadership.recruits'))
            ->has('nodeBuilder','>',0)
            ->has('walletPairing','>',(int) config('commissions.leadership.points'))
            ->get();

        return $users;
    }

    // Get leadership leadership fund weekly
    public function leadership_fund() {

        $today = Carbon::today()->subDay(1);

        $week_start = $today->startOfWeek()->format('Y-m-d H:i:s');
        $week_end = $today->endOfWeek()->format('Y-m-d H:i:s');

        $wallet = Wallet::where('description','LIKE','%WEEKLY LEADERSHIP POINTS COMMISSION%')
            ->whereBetween('created_at',[$week_start,$week_end])
            ->sum('amount');

        return $wallet;

    }

    // Get leadership leadership fund weekly
    public function get_amount_to_distribute() {

        $amount = 0;

        try {
            if($this->leadership_fund() > 0 && $this->users()->count() > 0) {
                $amount = $this->leadership_fund() / $this->users()->count();
            }
        }catch(Exception $e) {
            \Log::info('Leadership Dispatch Exceptio',[$e]);
        }


        return $amount;
    }

    // check if record exist
    public function isDispatched() {
        $today_start = Carbon::today()->startOfDay()->format('Y-m-d H:i:s');
        $today_end = Carbon::today()->endOfDay()->format('Y-m-d H:i:s');

        $wallet = Wallet::where([
            'source' => 'cashout',
            'status' => 'PROCESSING',
            'type' => 'Cr',
            'description' => 'YOUR WEEKLY LEADERSHIP BONUS',
        ])
            ->whereBetween('created_at',[$today_start,$today_end])
            ->count();
        if($wallet > 0) return true;
        return false;
    }

    public function distribute_funds() {
        // Get users in array
        $users = $this->users();
        foreach($users as $user) {
            $this->add_fund($user->id,$this->amount);
        }
    }

    public function add_fund($user_id,$amount) {
        Wallet::create([
            'source' => 'cashout',
            'status' => 'PROCESSING',
            'description' => 'YOUR WEEKLY LEADERSHIP BONUS',
            'currency' => config('currency.default'),
            'amount' => $amount,
            'type' => 'Cr',
            'user_id' => $user_id
        ]);
    }

    public function isTodayStartOfWeek() {
        if(Carbon::today()->format('Y-m-d') == Carbon::today()->startOfWeek()->format('Y-m-d'))
            return true;
        return false;
    }

}
