<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Wallet;
use Auth;

class UnilevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('unilevel.index',[
            'unilevel' => $this->unilevel(),
            'month_end' => Carbon::today()->endOfMonth()->format('Y-m-d'),
            'date_today' => Carbon::today()->format('Y-m-d')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function unilevel() {
        return Wallet::where(['user_id' => Auth::user()->id])
            ->where(['type' => 'Cr','status' => 'COMPLETED'])
            ->where('description','LIKE','%UNILEVEL COMMISSION%')
            ->sum('amount');
    }

    public function move() {

        $total = $this->get_sum('unilevel');
        $minimum = $this->get_minimum('unilevel');



        if($total >= $minimum && $this->isMonthEndToday()) {
            Wallet::where(['user_id' => Auth::user()->id,'source' => 'unilevel'])->update([
                'source' => 'cashout',
                'status' => 'PROCESSING'
            ]);
        }

        return redirect()->route('unilevel');
    }

    public function get_minimum($source) {
        return (int) config('withdraw.minimum.'.$source);
    }


    public function get_sum($source) {
        return Wallet::where([
            'user_id' => Auth::user()->id,'source' => $source])->sum('amount');
    }

    public function isMonthEndToday() {
        $month_end = Carbon::today()->endOfMonth()->format('Y-m-d');
        $date_today = Carbon::today()->format('Y-m-d');
        if($month_end == $date_today) return true;
        return false;
    }

}
