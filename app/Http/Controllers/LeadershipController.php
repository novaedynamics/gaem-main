<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Node;
use App\Wallet;
use Carbon\Carbon;

class LeadershipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('leadership.index',['leadership' => $this->leadership_bonus()]);
    }

    public function leadership_bonus() {

        $users = User::with(['childrenWeek','nodeBuilder','walletPairing'])
            ->has('childrenWeek','>', (int) config('commissions.leadership.recruits'))
            ->has('nodeBuilder','>',0)
            ->has('walletPairing','>',(int) config('commissions.leadership.points'))
            ->whereNotIn('username',['Gaemcorp01'])
            ->get();

        return $users;
    }

}
