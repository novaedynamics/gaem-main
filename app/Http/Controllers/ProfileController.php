<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.index');
    }

    public function setting()
    {
        return view('profile.setting');
    }

    public function me(Request $request) {
        $me = User::find(auth('api')->user()->id);
        return response()->json(compact('me'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_me(Request $request)
    {
        $request->validate([
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'email' => 'required|email'
        ]);

        if(auth()->user()->hasAnyRole(['admin','manager'])) {
            $attr = [
                "firstname" =>  $request->firstname,
                "middlename" =>  $request->middlename,
                "lastname" =>  $request->lastname,
                "email" =>  $request->email,
                "phone" =>  $request->phone,
                "address" =>  $request->address,
                "city" =>  $request->city,
                "postal" =>  $request->postal,
                "state" =>  $request->state,
                "country" =>  $request->country,
            ];
        } else {
            $attr = [
                "phone" =>  $request->phone,
                "address" =>  $request->address,
                "city" =>  $request->city,
                "postal" =>  $request->postal,
                "state" =>  $request->state,
                "country" =>  $request->country,
            ];
        }

        $me = User::where('id',auth('api')->user()->id)->update($attr);

        return response()->json(compact('me'));

    }

}
