<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Code;
use App\User;
use App\Transfer;
use App\Journal;
use Auth;

class CodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $starter = Code::where(['package_id' => 1,'status' => 'VALID'])->where('user_id',Auth::user()->id)->count();
        $builder = Code::where(['package_id' => 2,'status' => 'VALID'])->where('user_id',Auth::user()->id)->count();

        return view('codes.index',[
            'starter' => $starter,
            'builder' => $builder
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history()
    {

        $history_decode = $this->history_decode_data();

        return view('codes.history',[
            'transfers' => $this->history_decode_data()
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function transfer(Request $request)
    {

        if($request->ajax()) {

            $request->validate([
                'username' => 'required|exists:users,username',
                'quantity' => 'required|numeric|max:100|min:1',
                'packageId' => 'required|numeric',
            ]);

            $receiver = User::where('username',$request->username)->first();

            $available = Code::where(['user_id' => auth('api')->user()->id, 'package_id' => $request->packageId,'status' => 'VALID'])->count();
            $quantity = $request->quantity;

            if($available < $quantity)
                return response()->json(['message' => 'Insufficient quantity of available codes'], 422);

            $codes = Code::where([
                'status' => 'VALID',
                'user_id' => auth('api')->user()->id,
                'package_id' => $request->packageId])->limit($quantity)->get();

            $items = [];

            foreach($codes as $code)
                array_push($items,$code->id);

            $updated = Code::whereIn('id',$items)->update(['user_id' => $receiver->id]);
            Journal::create([
                'data' => json_encode([
                    'model' => "Code",
                    "type" => "Transfer",
                    "from" => ["user_id" => Auth::user()->id,'username' => Auth::user()->username],
                    "to" => ["user_id" => $receiver->id,'username' => $receiver->username],
                    "codes" => [
                        "quantity" => count($items),
                        "package_id" => $request->packageId,
                        'package_name' => \App\Package::find($request->packageId)->first()->name]
                ])
            ]);

            return response()->json(compact('updated'));
        }

        return view('codes.transfer');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request)
    {
        if($request->ajax()) {
            $request->validate([
                'quantity' => 'required|min:1|max:10',
                'package' => 'required|string',
            ]);

            for ($i = 0; $i < $request->quantity; $i++) {
                Code::create([
                    'code' => $i.time(),
                    'user_id' => auth('api')->user()->id,
                    'status' => 'VALID',
                    'package_id' => $request->package
                ]);
            }

            return response()->json(['message' => 'Codes generated!']);
        }


        return view('codes.generate');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function history_decode_data() {
        $me = Auth::user()->id;
        $history = Journal::
            where('data','LIKE','%{"model":"Code","type":"Transfer","from":{"user_id":'.$me.',%')
            ->orWhere('data','LIKE','%{"model":"Code","type":"Transfer",%%"to":{"user_id":'.$me.',%')
            ->limit(25)->orderBy('id','DESC')->get();
        foreach($history as $h) {
            $h->data = json_decode($h->data);
        }

        return $history;
    }

}
