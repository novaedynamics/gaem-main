<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Wallet;
use Carbon\Carbon;

class PoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function global()
    {

        $users = User::where('data','LIKE','%"pool":true%')
            ->whereHas('nodes',function($q) {
                $q->where(['package_id' => 2]);
            },'>',0)
            ->get();

        dd($users);

        return view('pool.global');
    }
}
