<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Cookie;

class SponsorController extends Controller
{

    public $user;

    public function sponsor($sponsor) {

        Cookie::forget('sponsor');
        $route = 'register';
        $cookie = cookie('sponsor',null,1);

        if($this->check_sponsor_exist($sponsor) > 0) {
            $route = "register";
            $cookie = cookie('sponsor',$sponsor, 60 * 24 * 7); // 1 day cookie validity
        }

//        dd($cookie);

        return redirect()->route($route)->withCookie($cookie);

    }

    protected function check_sponsor_exist($sponsor) {
        return User::where('username',$sponsor)->count();
    }

    public function summary() {
        return response()->json(User::find($this->user->parent_id));
    }

}
