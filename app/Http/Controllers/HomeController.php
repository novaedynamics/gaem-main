<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Wallet;
use App\User;
use App\Node;
use App\Code;
use Auth;
use Psy\Input\CodeArgument;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function dashboard() {

        return view('dashboard',[
            'summary' => $this->summary(),
            'members' => User::whereNotIn('id',[1,2,3])->limit(6)->orderBy('id','DESC')->get(),
            'leadership_qualifiers' => $this->leadership_qualifiers(),
            'sponsor' => $this->get_sponsor()
        ]);
    }

    public function credit() {
        return Wallet::where(['user_id' => Auth::user()->id,'type' => 'Cr'])->sum('amount');
    }

    public function debit() {
        return Wallet::where(['user_id' => Auth::user()->id,'type' => 'Dr'])->sum('amount');
    }

    public function balance() {
        return $this->credit() - $this->debit();
    }

    public function bonus() {
        return Wallet::where(['user_id' => Auth::user()->id,'source' => 'bonus'])->sum('amount');
    }

    public function pairing() {
        return Wallet::where(['user_id' => Auth::user()->id])
                ->where('description','LIKE','%MATCH COMMISSION%')
                ->sum('amount');
    }

    public function referral() {
        return  $referral = Wallet::where(['user_id' => Auth::user()->id])
                ->where('description','LIKE','%REFERRAL COMMISSION%')
                ->sum('amount');
    }

    public function unilevel() {
        return Wallet::where(['user_id' => Auth::user()->id])
            ->where('description','LIKE','%UNILEVEL COMMISSION%')
            ->sum('amount');
    }

    public function product() {
        return Wallet::where(['user_id' => Auth::user()->id,'source' => 'product'])->sum('amount');
    }

    public function payouts() {
        return Wallet::where(['user_id' => Auth::user()->id,
            'source' => 'cashout',
            'status' => 'COMPLETED',
            'type' => 'Dr'])->sum('amount');
    }

    public function travelFunds() {
        return Wallet::where(['user_id' => Auth::user()->id,'source' => 'travel','status' => 'COMPLETED'])->sum('amount');
    }

    public function followers() {
        return User::where('parent_id',Auth::user()->id)->count();
    }

    public function codes() {
        return Code::where(['user_id' => Auth::user()->id,'status' => 'VALID'])->count();
    }

    public function cashout() {

        $total = 0;

        $total += Wallet::where(['user_id' => Auth::user()->id,
            'source' => 'cashout',
            'status' => 'PROCESSING',
            'type' => 'Cr'])
            ->sum('amount');

        $total += Wallet::where(['user_id' => Auth::user()->id,
            'source' => 'cashout',
            'description' => 'MOVE TO MAIN WALLET',
            'status' => 'COMPLETED'])
            ->sum('amount');

        return $total;
    }

    public function cashout_request() {

        $total = 0;

        $total += Wallet::where(['user_id' => Auth::user()->id,
            'source' => 'cashout',
            'status' => 'PROCESSING',
            'type' => 'Dr'])
            ->sum('amount');

        $total += Wallet::where(['user_id' => Auth::user()->id,
            'source' => 'cashout',
            'status' => 'COMPLETED',
            'type' => 'Dr'])
            ->sum('amount');

        return $total;
    }

    public function royalty() {
        return Wallet::where(['user_id' => Auth::user()->id])
                ->where('description','LIKE','%ROYALTY COMMISSION%')
                ->sum('amount');
    }

    // weekly leadership
    public function leadership_fund() {

        $week_start = Carbon::today()->startOfWeek()->format('Y-m-d H:i:s');
        $week_end = Carbon::today()->endOfWeek()->format('Y-m-d H:i:s');

        $wallet = Wallet::where('description','LIKE','%WEEKLY LEADERSHIP POINTS COMMISSION%')
            ->whereBetween('created_at',[$week_start,$week_end])
            ->sum('amount');

        return $wallet;
    }

    public static function total_commission() {
        return Wallet::where(['type' => 'Cr','user_id' => Auth::user()->id])
            ->whereNotIn('description',['MOVE TO MAIN WALLET'])
            ->sum('amount');
    }

    public function summary() {
        $credit = $this->credit();
        $debit = $this->debit();
        $balance = $this->balance();
        $bonus = $this->bonus();
        $pairing = $this->pairing();
        $invites = $this->referral();
        $unilevel = $this->unilevel();
        $product = $this->product();
        $payouts = $this->payouts();
        $followers = $this->followers();
        $codes = $this->codes();
        $travel = $this->travelFunds();
        $cashout_request = $this->cashout_request();
        $cashout = $this->cashout() - $cashout_request;
        $royalty = $this->royalty();
        $global_pool = Wallet::global_pool();
        $total_commission = $this->total_commission();
        $leadership_fund = $this->leadership_fund();

        return compact('followers','credit','debit','balance','royalty','global_pool','leadership_fund',
            'bonus','pairing','invites','unilevel','product','payouts','codes','travel','cashout','total_commission');

    }

    public function leadership_qualifiers() {

        $users = User::with(['childrenWeek','nodeBuilder','walletPairing'])
            ->has('childrenWeek','>', (int) config('commissions.leadership.recruits'))
            ->has('nodeBuilder','>',0)
            ->has('walletPairing','>',(int) config('commissions.leadership.points'))
            ->whereNotIn('username',['Gaemcorp01'])
            ->get();

        return $users;
    }

    public function get_sponsor() {
        return User::find(auth()->user()->parent_id);
    }

}
