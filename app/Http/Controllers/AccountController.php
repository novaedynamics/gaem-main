<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Events\ActivateAccount;
use App\Events\ActivateNode;
use App\Events\Orders\ActivateOrder;
use App\Node;
use App\Code;
use App\User;
use App\Wallet;
use App\Role;
use App\Package;
use Carbon\Carbon;
use Auth;
use Image;

class AccountController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->ajax()) {
            $accounts = Node::with(['package','wallet' => function($q) {
                $q->where(['source' => 'pairing','type' => 'Cr'])->count();
            },''])->where('user_id',auth('api')->user()->id)->get();
        } else {
            $accounts = Node::with(['package','wallet' => function($q) {
                $q->where(['source' => 'pairing','type' => 'Cr'])->count();
            }])->where('user_id',Auth::user()->id)->get();
        }

//        dd($accounts);

        return view('accounts.index',[
            'accounts' => $accounts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request)
    {

        if($request->ajax()) {

            $user = auth('api')->user();

            $request->validate([
                'package' => 'required',
            ]);

            $code_count = Code::where(['package_id' => $request->package, 'user_id' => $user->id])->count();

            if(!$user->hasRole('inactive'))
                return response()->json(['message' => 'Your account is already active!'],200);

            if($code_count < 1)
                return response()->json(['message' => 'Unable to activate account!'],422);

            $package = Package::find($request->package);

            event(new ActivateAccount(User::find(auth('api')->user()->id),$package));

            return response()->json(['message' => 'Your account is now activated!'],200);
        }

        return view('accounts.activate',[
            'codes' => $this->get_codes()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->ajax()) {

            $request->validate([
                'firstname' => 'required|string|max:255',
                'lastname' => 'required|string|max:255',
                'username' => 'required|string|unique:users,username|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
                'phone' => 'required',
                'position' => 'required|string',
                'nodeId' => 'required|numeric',
                'packageId' => 'required|numeric'
            ]);

            $count = Code::where([
                'user_id' => Auth::user()->id,
                'package_id' => $request->packageId,
                'status' => 'VALID'])->count();

            $leg_null = Node::where(['id' => $request->nodeId])->first();

            if($count < 1) return response()->json(['message' => 'Unable to process request','code' => 'Code not available'],422);
            if($leg_null[$request->position] != NULL) return response()->json(['message' => 'Unable to process request','leg' => 'Leg is not empty'],422);

            $package = Package::find($request->packageId);

            // Create new account
            $user = User::create([
                'username' => $request->username,
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'phone' => $request->phone,
                'parent_id' => Auth::user()->id,
                'password' => Hash::make($request->password),
            ]);

            $user->roles()->attach(Role::where('name', 'regular')->first());

            // New node account
            if($user) {

                $attr = [
                    'user_id' => $user->id,
                    'parent_id' => $request->nodeId,
                    'package_id' => $request->packageId,
                    'position' => $request->position,
                    'name' => $user->username.'_'. time()];

                $node = Node::create($attr);

                event(new ActivateNode(Auth::user(), $package, $attr,$node));

            }

            return response()->json(['message' => 'Your account has been created!','type' => 'POST']);

        }



        return response()->json(['message' => 'Your account has been created!']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = Node::where('user_id',Auth::user()->id)->where('id',$id)->first();

        return view('accounts.edit',[
            'account' => $account,
            'accountName' => $account->name,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->ajax()) {

            $request->validate([
                'name' => 'required|string'
            ]);

            $account = Node::where('id',$id)->where('user_id',auth('api')->user()->id)->first();
            $account->update(['name' => $request->name]);

            return response()->json(compact('account'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_my_password(Request $request)
    {
        $request->validate([
            'current_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
        ]);

        $user = User::find(auth('api')->user()->id);
        if (!Hash::check($request->current_password, $user->password)) {
            return response()->json(['message' => 'Invalid current password','pw' => $request->current_password],422);
        }

        $user = User::find(auth('api')->user()->id);
        $user->password = Hash::make($request->new_password);
        $user->save();

        return response()->json(['message' => 'Password has been update.']);
    }

    public function updateAvatar(Request $request) {

        if($request->avatar) {

            if($this->check_mime_image($request->avatar))
                return response()->json(['message','Unable to update avatar'], 422);

            $pos  = strpos($request->avatar, ';');
            $type = str_replace('image/','',explode(':', substr($request->avatar, 0, $pos))[1]);
            $filename = time().'.'.$type;
            Image::make($request->avatar)->resize(200,200)->save(public_path('media/avatars/'.$filename));

            $user = User::find(auth('api')->user()->id);
            $user->avatar = '/media/avatars/'.$filename;
            $user->save();

            return response()->json(['message' => 'Avatar has been updated']);
        }

        return response()->json(['message' => 'Unable to update avatar'],422);

    }

    protected function check_mime_image($type) {
        if($type == 'image/jpg') return true;
        if($type == 'image/jpeg') return true;
        if($type == 'image/png') return true;
        if($type == 'image/gif') return true;
        return false;
    }

    private function get_codes() {

        $codes = ['SP' => 0, 'BP' => 0, 'AP' => 0, 'total' => 0];

        $codes['SP'] = Code::where(['user_id' => Auth::user()->id, 'package_id' => 1])->count();
        $codes['BP'] = Code::where(['user_id' => Auth::user()->id, 'package_id' => 2])->count();
        $codes['AP'] = Code::where(['user_id' => Auth::user()->id, 'package_id' => 3])->count();
        $codes['total'] =  (int) $codes['SP'] + (int) $codes['BP'] + (int) $codes['AP'];

        return json_encode($codes);
    }

    public function transfer($name) {
        $node = Node::where('name',$name)->first();
        $amount = Wallet::where(['type' =>'Cr','source' => 'pairing','node_id' => $node->id])->sum('amount');
        return view('accounts.transfer',['amount' => $amount,'node' => $node]);
    }

    public function move_to_wallet($name) {

        $node = Node::where([
            'name' => $name,
            'user_id' => Auth::user()->id])->first();

        $amount = Wallet::where([
            'node_id' => $node->id,
            'user_id' => Auth::user()->id,
            'source' => 'pairing'])
            ->sum('amount');

        Wallet::create([
            'currency' => 'PHP',
            'amount' => $amount,
            'type' => 'Cr',
            'status' => 'COMPLETED',
            'source' => 'cashout',
            'description' => 'MOVE TO MAIN WALLET',
            'user_id' => Auth::user()->id
        ]);

        Wallet::where(['node_id' => $node->id, 'user_id' => Auth::user()->id,'source' => 'pairing'])
            ->update(['source' => 'cashout','status' => 'COMPLETED']);

        return redirect()->route('wallet');
    }

}
