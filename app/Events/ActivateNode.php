<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\User;
use App\Node;
use App\Package;


class ActivateNode
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    public $attr;

    public $node;

    public $package;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user,Package $package, $attr, Node $node)
    {
        $this->user = $user;
        $this->package = $package;
        $this->attr = $attr;
        $this->node = $node;
    }

}
