<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Node;
use App\User;


class TravelFund
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;

    public $node;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Node $node, User $user)
    {
        $this->user = $user;
        $this->node = $node;
    }

}
