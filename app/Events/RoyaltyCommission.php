<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Node;
use App\User;

class RoyaltyCommission
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $node;

    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Node $node)
    {
        $this->node = $node;                        // new created node
        $this->user = User::find($node->user_id);   // user of the new node
    }

}
