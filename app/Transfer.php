<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    public $fillable = [
        'desc',
        'package_id',
        'user_id'
    ];

    public function user() {
        return $this->belongsTo(App\User::class);
    }

    public function package() {
        return $this->belongsTo(\App\Package::class);
    }


}
