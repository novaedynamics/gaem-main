<?php

namespace App\Listeners\ActivateAccount;

use App\Events\ActivateAccount;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;
use App\Role;
use App\User;
use App\Code;

class UpdateRole
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  ActivateAccount  $event
     * @return void
     */
    public function handle(ActivateAccount $event)
    {
        Log::info('Update Role', [$event->user]);
        $this->update_role($event->user);
    }

    private function update_role(User $user) {

        $role_regular  = Role::where('name', 'regular')->first();
        $user = User::find($user->id);

        if($user->hasRole('inactive')) {

            $user->roles()->detach();
            $user->roles()->attach($role_regular);

            $code = Code::where('user_id',$user->id)->first();
            $code->status = 'USED';
            $code->save();

        }

    }

}
