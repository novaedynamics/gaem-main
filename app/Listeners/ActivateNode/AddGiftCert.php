<?php

namespace App\Listeners\ActivateNode;

use App\Events\ActivateNode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Gift;
use App\Package;

class AddGiftCert
{

    public $user;
    public $package;
    public $attr;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActivateNode  $event
     * @return void
     */
    public function handle(ActivateNode $event)
    {

        if($event->package->code == 'SP') {
            $gc_value = config('packages.SP.addOn.gc.value');
            $gc_quantity = config('packages.SP.addOn.gc.quantity');
            factory(Gift::class,$gc_quantity)->create(['user_id' => $event->attr['user_id'],'amount' => $gc_value]);
        }
        if($event->package->code == 'BP') {
            $gc_value = config('packages.BP.addOn.gc.value');
            $gc_quantity = config('packages.BP.addOn.gc.quantity');
            factory(Gift::class,$gc_quantity)->create(['user_id' => $event->attr['user_id'],'amount' => $gc_value]);
        }
        if($event->package->code == 'AP') {
            $gc_value = config('packages.AP.addOn.gc.value');
            $gc_quantity = config('packages.AP.addOn.gc.quantity');
            factory(Gift::class,$gc_quantity)->create(['user_id' => $event->attr['user_id'],'amount' => $gc_value]);
        }

    }



}
