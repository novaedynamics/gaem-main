<?php

namespace App\Listeners\ActivateNode;

use App\Events\ActivateNode;
use App\Events\ReferralCommission;
use App\Events\RoyaltyCommission;
use App\Events\TravelFund;
use App\Package;
use App\Wallet;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Node;
use Carbon\Carbon;

class AddNode
{

    public $user;

    public $attr;

    public $trav_count;



    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActivateNode  $event
     * @return void
     */
    public function handle(ActivateNode $event)
    {
        $this->add_node($event);
    }

    public function add_node($event) {

        $node = $event->node;
        $parent = Node::find($event->attr['parent_id']);

        if($event->attr['position'] == 'lft') $parent->setLft($node->id);
        if($event->attr['position'] == 'rgt') $parent->setRgt($node->id);

        $this->process_points($node,$this->get_points($node->package_id));

        event(new ReferralCommission(User::find($node->user_id),$node));

        return $this;
    }

    // $node newly created; $points to add to ancestors
    public function process_points(Node $node, $points) {

        if($node->parent_id == NULL) return;

        $parent_node = Node::find($node->parent_id);

        $olft = $parent_node->lft_pts;
        $orgt = $parent_node->rgt_pts;

        if( $node->id == $parent_node->lft ) {

            if($parent_node->rgt_pts == 0) {

                $parent_node->lft_pts += $points;
                $parent_node->save();

            } elseif($parent_node->rgt_pts > 0) {

                // new rpoint value
                $rpoints = $points - $parent_node->rgt_pts;
                if($rpoints < 0) $rpoints = 0;

                // current node remaining poitns to add
                $lpoints = $parent_node->rgt_pts - $points;
                if($parent_node->rgt_pts - $points < 0) $lpoints = 0;

                $parent_node->lft_pts = $rpoints;
                $parent_node->rgt_pts = $lpoints;
                $parent_node->save();

                $this->add_match_count($parent_node,($orgt - $lpoints));

            }

        }

        if( $node->id == $parent_node->rgt ) {

            if($parent_node->lft_pts == 0) {

                $parent_node->rgt_pts += $points;
                $parent_node->save();

            } elseif($parent_node->lft_pts > 0) {

                // new rpoint value
                $lpoints = $points - $parent_node->lft_pts;
                if($lpoints < 0) $lpoints = 0;

                // current node remaining points to add
                $rpoints = $parent_node->lft_pts - $points;
                if($parent_node->lft_pts - $points < 0) $rpoints = 0;

                $parent_node->lft_pts = $rpoints;
                $parent_node->rgt_pts = $lpoints;
                $parent_node->save();

                $this->add_match_count($parent_node,($olft - $rpoints));

            }
        }

        $this->process_points($parent_node,$points);

    }

    // verify match points max daily
    public function isMaxMatched(Node $node,$max = 0) {
        $data = json_decode($node->data);
        if($max > $data->match->count) return false;
        return true;
    }

    public function get_node_match_count(Node $node) {
        $data = json_decode($node->data);
        return $data->match->count;
    }

    public function add_match_count(Node $node,$points = 0) {

        // $node; where you want to add the commission

        if($this->get_node_match_count($node) > $this->get_max_matched($node->package_id)) return;

        $data = json_decode($node->data);

        while($points > 0) {

            \Log::info('Node Today Data', ['match' => $data->match]);

            if(!$this->isDateToday($data->match->updated_at->date)) {
                $data->match->count = 0;
                $data->match->updated_at = Carbon::today();
            }

            if($data->match->count < $this->get_max_matched($node->package_id)) {
                $data->match->count++;
            }
            if($data->match->count % 5 == 0) {
                event(new TravelFund($node,User::find($node->user->id)));
            } else {

                $this->add_cash($node, 100);
                $this->add_royalty($node, 10);
                $node->balance += 100;

            }
            $points--;
        }
        $node->data = json_encode($data);
        $node->save();
    }

    public function reduceValue($value = 0 ,$points = 0) {
        if($value == 0) return $value;
        while ($points > 0) {
            if( $value == 0 && $points > 0 ) {
                return $points;
            }
            $value--;
            $points--;
        }
        return $value;
    }

    public function add_cash(Node $node, $amount = 0) {
        return Wallet::create([
            'source' => 'pairing',
            'description' => 'MATCH COMMISSION',
            'amount' => $amount,
            'type' => 'Cr',
            'currency' => "PHP",
            'status' => "COMPLETED",
            'user_id' => $node->user->id,
            'node_id' => $node->id
        ]);
    }

    public function add_royalty(Node $node, $amount = 0) {

        if(!$node->user->parent_id) return;

        return Wallet::create([
            'source' => 'royalty',
            'description' => 'ROYALTY COMMISSION',
            'amount' => $amount,
            'type' => 'Cr',
            'currency' => "PHP",
            'status' => "COMPLETED",
            'user_id' => $node->user->parent_id,
            'node_id' => $node->id
        ]);
    }

    protected function get_points($package_id) {
        if($package_id == 1) return config('packages.SP.points');
        if($package_id == 2) return config('packages.BP.points');
    }

    protected function get_max_matched($package_id) {
        if($package_id == 1) return config('commissions.match.SP.match.max');
        if($package_id == 2) return config('commissions.match.BP.match.max');
    }

    public function get_pair_count($node_id) {
        return Wallet::where(['node_id' => $node_id])
            ->whereDate('created_at',Carbon::today())->count();
    }

    public function isPairLessThenMax($node) {
        $pair_max = $this->get_max_matched($node->package_id);
        $pair_count = $this->get_pair_count($node->id);
        if($pair_count < $pair_max) {
            return true;
        }
        return false;
    }

    public function isDateToday($date) {
        if(Carbon::parse($date) == Carbon::today()) return true;
        return false;
    }

}
