<?php
/**
 * Created by PhpStorm.
 * User: moreishi
 * Date: 2019-03-01
 * Time: 04:22
 */

namespace App\Listeners\ActivateNode;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Node;
use App\User;

class Points
{
    public $node;

    public $parent;

    public $points;

    public function __construct(Node $node) {
        $this->node = $node;
    }

    public function setPoints($points) {
        $this->node->lft_pts += $points;
    }

}