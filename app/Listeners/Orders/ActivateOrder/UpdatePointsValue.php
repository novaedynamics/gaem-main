<?php

namespace App\Listeners\Orders\ActivateOrder;

use App\Events\Orders\ActivateOrder;
use App\Product;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Order;
use App\Wallet;
use App\User;
use Auth;

class UpdatePointsValue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActivateOrder  $event
     * @return void
     */
    public function handle(ActivateOrder $event)
    {
        $this->execute_points_value($event);
    }

    public function execute_points_value($event) {
        $depth = (int) config('commissions.unilevel.depth');
        $this->add_points_value($event->user,$event->order,$depth);
        $this->add_cash_from_global_pool($this->get_product_quantity($event->order) * 5);
    }

    public function add_points_value($user,$order,$depth) {

        if($depth < 1) return;

        $pv_date = Carbon::parse(json_decode($user->data)->updated_at);

        if($pv_date->format('m') != Carbon::today()->format('m')) {

            $data = json_decode($user->data);

            $data->activated = ($data->pv >= 160) ? true : false;
            $data->pool = ($data->pv >= 320) ? true : false;
            $data->pv = 0;
            $data->updated_at = Carbon::today()->format('Y-m-d H:i:s');

            $user->pv = 0; // current user receives points value
            $user->data = json_encode($data);
            $user->save();

        }

        $points = $this->compute_points_value($order);

        if(json_decode($user->data)->activated == true || $user->id == Auth::user()->id) {

            $data = json_decode($user->data);
            $data->pv += $points;
            $data->updated_at = Carbon::now()->format('Y-m-d H:i:s');

            $this->add_cash_from_pv($user->id,$points);
            $user->pv += $points; // current user receives points value
            $user->data = json_encode($data);
            $user->save();

            $this->update_order_status($order->id);

        }

        if($user->parent_id == NULL) return;

        $user_parent = User::find($user->parent_id);

        $count = $depth-1;

        $this->add_points_value($user_parent,$order,$count);

    }

    /* Compute points value to add
     * */
    protected function compute_points_value($order) {

        $points = 0;

        $order = json_decode($order->data);

        $scalar = $this->get_stock_points_value('scalar') * $order->scalar;
        $mega_rub = $this->get_stock_points_value('mega_rub') * $order->mega_rub;
        $coffee = $this->get_stock_points_value('coffee') * $order->coffee;
        $vaporeen = $this->get_stock_points_value('vaporeen') * $order->vaporeen;

        $points = (($scalar + $mega_rub + $coffee + $vaporeen) * (int) config('commissions.unilevel.commission.percent')) / 100;


        return $points;

    }

    /* Compute points value to add
     * */
    protected function get_product_quantity($order) {

        $quantity = 0;

        $order = json_decode($order->data);

        $scalar = $order->scalar;
        $mega_rub = $order->mega_rub;
        $coffee = $order->coffee;
        $vaporeen = $order->vaporeen;

        $quantity = ($scalar + $mega_rub + $coffee + $vaporeen);

        return $quantity;

    }

    protected function get_stock_points_value($stock) {
        $value = 0.00;
        if($stock == 'scalar') $value = Product::where('sku',$stock)->first()->pv;
        if($stock == 'mega_rub') $value = Product::where('sku',$stock)->first()->pv;
        if($stock == 'coffee') $value = Product::where('sku',$stock)->first()->pv;
        if($stock == 'vaporeen') $value = Product::where('sku',$stock)->first()->pv;
        return (float) $value;
    }

    protected function add_cash_from_pv($user_id,$amount) {
        return Wallet::create([
            'source' => 'unilevel',
            'amount' => $amount,
            'description' => 'UNILEVEL COMMISSION',
            'currency' => "PHP",
            'type' => 'Cr',
            'status' => 'COMPLETED',
            'user_id' => $user_id
        ]);
    }

    protected function add_cash_from_global_pool($amount) {
        return Wallet::create([
            'source' => 'global-pool',
            'amount' => $amount,
            'description' => 'GLOBAL POOL COMMISSION',
            'currency' => "PHP",
            'type' => 'Cr',
            'status' => 'COMPLETED'
        ]);
    }

    protected function update_order_status($order_id) {
        Order::find($order_id)->update(['status' => 'USED']);
    }

}
