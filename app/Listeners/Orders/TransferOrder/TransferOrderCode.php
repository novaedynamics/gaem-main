<?php

namespace App\Listeners\Orders\TransferOrder;

use App\Events\Orders\TransferOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Code;

class TransferOrderCode
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TransferOrder  $event
     * @return void
     */
    public function handle(TransferOrder $event)
    {
        $this->transfer($event);
    }

    public function transfer($event) {
        $event->order->update(['user_id' => $event->user->id]);
    }

}
