<?php

namespace App\Listeners\RoyaltyCommission;

use App\Events\RoyaltyCommission;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Node;
use App\User;
use App\Wallet;
class AddRoyaltyCommission
{

    public $node;

    public $user;

    public $node_parent;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RoyaltyCommission  $event
     * @return void
     */
    public function handle(RoyaltyCommission $event)
    {
        // event user
        $this->user = $event->user;
        // event node
        $this->node = $event->node;
        // parent node
        $parentNode = Node::find($event->node->parent_id);

        $this->execute_royalty_commission( $parentNode);
    }

    public function execute_royalty_commission( Node $parentNode) {
        if($this->is_parent_legs_filled( $parentNode)) {
            $this->trigger_commission( $parentNode);
        }
    }

    // Does the parent legs filled? Boolean
    public function is_parent_legs_filled(Node $parentNode) {
        if( $parentNode->lft != NULL &&  $parentNode->rgt != NULL) {
            return true;
        }
        return false;
    }

    // get parent's parent node;
    // trigger commission
    public function trigger_commission(Node $parentNode) {

        // parent sponsor
        $sponsor =  User::find( $parentNode->user_id)->parent_id;

        if(!$sponsor) return;

        $points = $this->get_package_points($parentNode->package_id);

        // Get royalty commission value
        $value = config('commissions.royalty.value');

        while($points > 0) {

            \Log::info('Royalty points',['points' => $points]);
            \Log::info('Royalty Count',['count' => $this->get_royalty_count(['type' => 'Cr','source' => 'royalty'])]);

//            if(!in_array($this->get_royalty_count(['type' => 'Cr','source' => 'royalty']),[5,10,15,20,25])) {
                $this->add_cash($value,$sponsor,$parentNode->id);
//            }
            $points--;
        }

    }

    public function get_package_points($package_id) {
        $points = 0;
        if($package_id == 1) $points = config('packages.SP.points');
        if($package_id == 2) $points = config('packages.BP.points');
        return $points;
    }

    public function add_cash($value,$sponsorId,$nodeId) {
         Wallet::create([
            'source' => 'royalty',
            'description' => 'ROYALTY COMMISSION',
            'amount' => $value,
            'type' => 'Cr',
            'currency' => 'PHP',
            'status' => 'COMPLETED',
            'user_id' => $sponsorId,
            'node_id' => $nodeId,
        ]);
    }

    public function get_royalty_count($att = []) {
        return Wallet::where($att)->count();
    }

}
