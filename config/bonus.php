<?php
/**
 * Created by PhpStorm.
 * User: moreishi
 * Date: 2019-02-19
 * Time: 22:48
 */

return [
    'direct' => [
        'depth' => 12,
    ],
    'referral_commission' => [
        'SP' => [
            'depth' => 3,
            'cash' => 0.00,
            'gc' => [
                'value' => 100.00,
                'quantity' => 2
            ]
        ],
        'BP' => [
            'depth' => 3,
            'cash' => 150.00,
            'gc' => [
                'value' => 100.00,
                'quantity' => 3
            ]
        ],
        'AP' => [
            'depth' => 3,
            'cash' => 0.00,
            'gc' => [
                'value' => 0.00,
                'quantity' => 0
            ]
        ],
    ]
];