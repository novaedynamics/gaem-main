<?php
/**
 * Created by PhpStorm.
 * User: moreishi
 * Date: 2019-03-03
 * Time: 08:28
 */
return [
    'recruits' => 1, // more than 1 recruit
    'points' => 49, // Match points 50 or more
    'SP' => ['amount' => 18.00],
    'BP' => ['amount' => 28.00],
];